angular.module('olymp')
  .config(function($routeProvider){
    $routeProvider
	.when('/', {controller: 'AccueilCtrl', templateUrl: 'accueil.html'})
  	.when('/participer', {controller: 'ParticiperCtrl',templateUrl:'participer.html'})
	.when('/infos', {controller: 'InfosCtrl', templateUrl:'infos.html'})
	.when('/admin', {controller: 'AdminCtrl', templateUrl:'admin.html'})

})
