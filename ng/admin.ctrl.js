angular.module('olymp')
.controller('AdminCtrl',function($scope,EquipesSvc){
	//récupération des équipes
	EquipesSvc.fetchAdmin().success(function(retour){
		$scope.equipes=retour
	})

	//récupération de la présence d'un fichier
	$scope.onFileSet=function(files){
		console.log("fichier chargé")
		$scope.image=files[0]
	}


	//ajout de l'équipe lors du clic
	$scope.addEquipe=function(){
		if($scope.nom){
			console.log("ajout d'une équipe avec le fichier" + $scope.image)
			EquipesSvc.create({
				nom:$scope.nom,
				image:$scope.image
			}).success(function(equipe){
				$scope.equipes.unshift(equipe)
				$scope.nom=null
			})
		}
	}



})
