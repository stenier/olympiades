angular.module('olymp')
.service('EquipesSvc', function($http){
	this.fetchAdmin=function(){
		return $http.get('/api/adminequipes')
	}

	this.fetch=function(){
		return $http.get('/api/equipes')
	}

	this.create=function(equipe){
		var fd=new FormData()
		fd.append('nom',equipe.nom)
		fd.append('image',equipe.image)
		return $http({
			method: 'POST',
			url: '/api/equipes',
			data: fd,
			headers:{'Content-type':undefined},
			transformRequest: function(data, headersGetterFunction){
				return data
			}
		})
	}

	this.addParticipant=function(equipe){
		return $http.post('/api/rejoindre', equipe)
	}
})
.factory("MessageSrv", function(){
	return{}
})
//http://stackoverflow.com/questions/25847467/send-image-jpeg-along-with-other-form-data-in-angularjs
