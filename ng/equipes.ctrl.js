angular.module('olymp')
.controller('AccueilCtrl',function(){})
.controller('InfosCtrl',function($scope, MessageSrv){
	//si quelqu'un vient de s'inscrire, MessageSrv contient un attribut nom
	$scope.Message=MessageSrv
})
.controller('ParticiperCtrl',function($scope,$location,EquipesSvc, MessageSrv){
	//validation des nombres
	$scope.isNumber=angular.isNumber
	//variable de validité du formulaire
	$scope.formValide=false
	//valeurs par défaut du formulaire
	$scope.enfants=0
	$scope.adultes=0
	//filtrage des équipes dispos en fonction des participants
	$scope.filtrerDispos=function(){
		console.log('filtrage')
		$scope.formValide=false
		if (($scope.enfants>=0)&&($scope.adultes>=0)&&($scope.adultes+$scope.enfants>1)){
			if ($scope.nom && $scope.nom.length>0){
				$scope.formValide=true
			}else{
				$scope.formValide=false
			}

			console.log('les valeurs sont définies, calcul des équipes complètes')
			for (var i in $scope.equipes){
				var eq=$scope.equipes[i]
				if(8-eq.nbInscrits-$scope.enfants-$scope.adultes<0){
					eq.completable=false
					console.log('L\'équipe '+eq.nom+" est complète")
				}else{
					eq.completable=true
				}
				//on ne complète pas avec une seule personne
				if (eq.nbInscrits>=7){
					eq.completable=false
				}	
			}

		}
	}
	//ajout de participants à l'équipe
	$scope.rejoindre=function(idEquipe){
		console.log('préparation de lajout dun participant pour '+idEquipe)
		var valide=true
		if($scope.nom==null || $scope.enfants==null || $scope.adultes==null){
			$scope['err'+idEquipe]=true
		}else{
			EquipesSvc.addParticipant({
				idEquipe:idEquipe,
				nom:$scope.nom,
				nbEnfants:$scope.enfants,
				nbAdultes:$scope.adultes
			})
			.success(function(equipe){
				$location.path('infos')
				$scope.message=MessageSrv
				$scope.message.nom=$scope.nom
			})
		}
	}	

	//récupération des équipes
	EquipesSvc.fetch().success(function(retour){
		$scope.equipes=retour
		for (var i in $scope.equipes){
			var eq=$scope.equipes[i]
			if (eq.nbInscrits>=7)
				eq.completable=false
		}
	})
})

