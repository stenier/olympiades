var Equipe=require('../../models/equipes')
var router=require('express').Router()

var gm = require('gm');

var inspect = require('util').inspect;
var Busboy=require('busboy')

router.get('/images/:refimage', function(req, res, next){
	Equipe.findOne({"nom":req.params.refimage}, function(ref,champ){
		if(champ!=null && champ.image.length>0){
			res.set("Content-type",champ.image[0].contentType)
			res.send(champ.image[0].data)
		}else{
			res.status(404).send("pas d'image")
		}
	})
})

//récupération des équipes côté participants
router.get('/api/equipes', function (req, res, next) {
	Equipe.find()
	.exec(function(err,equipes){
		//vérification que mongo renvoie bien des équipes
		if (err) {return next(err)}
		//creation du tableau de données pertinentes à retourner
		var retour=[]
		//calcul du nombre de places par équipes
		for (var eqIdx in equipes){
			var origine=equipes[eqIdx]
			var calcul=0 //nombre total d'inscrits 
			//parcours des inscrits
			for (var i=0; i < origine.participants.length; i++){
				var par=origine.participants[i]
				calcul+=par.nbEnfants+par.nbAdultes
			}
			retour.push({
				id:origine._id
				,nom:origine.nom
				,nbInscrits:calcul
				,completable:origine.completable
//				,participants:origine.participants
			})
		}
		//renvoi du json
		res.json(retour)
	})
})


//récupération des équipes côté admin
router.get('/api/adminequipes', function (req, res, next) {
	Equipe.find()
	.exec(function(err,equipes){
		//vérification que mongo renvoie bien des équipes
		if (err) {return next(err)}
		//creation du tableau de données pertinentes à retourner
		var retour=[]
		//calcul du nombre de places par équipes
		for (var eqIdx in equipes){
			var origine=equipes[eqIdx]
			var calcul=0 //nombre total d'inscrits 
			//parcours des inscrits
			for (var i=0; i < origine.participants.length; i++){
				var par=origine.participants[i]
				calcul+=par.nbEnfants+par.nbAdultes
			}
			retour.push({
				id:origine._id
				,nom:origine.nom
				,nbInscrits:calcul
				,completable:origine.completable
				,participants:origine.participants
			})
		}
		//renvoi du json
		res.json(retour)
	})
})

router.post('/api/rejoindre',function(req, res, next){
	console.log('traitement de l ajout du participant dans le controlleur')
	var idEquipe=req.body.idEquipe
	console.log('Léquipe est '+idEquipe)
	Equipe.findOne({_id : idEquipe},function(err,equipe){
		if(err){console.log('Erreur de récupération de l\'équipe')
			res.json(500,'impossible de récupérer l\'équipe')
			return next(err)
		}else{
			console.log('Equipe '+equipe.nom + ' chargée dans le controlleur')
			equipe.participants.push({
				nom:req.body.nom,
				nbEnfants:req.body.nbEnfants,
				nbAdultes:req.body.nbAdultes
			})
			equipe.save(function (err, resultat) {
				if (err) { return next(err) }
				res.status(201).json(resultat.participants)
			})
		}
	})
})

router.post('/api/equipes', function(req, res, next){
	var busboy= new Busboy({headers: req.headers})
	var buffers=[];//tableau pour récupérer les chunks de données
	var equipe=new Equipe();//equipe vide
	//règle de récupération de l'image avec busboy
	busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
		file.on('data', function(data) {
			buffers.push(data)
		});
		file.on('end', function() {
			var bufferFinal=Buffer.concat(buffers);
			//génération du thumbnail
			console.log('génération du thumbnail')
			gm(bufferFinal)
			.resize(200,200)
			.encoding('None')
			.stream(function(err,stdout,stderr){
				if(!err){
					var buf=new Buffer(0)
					stdout.on('data',function(d){
						buf=Buffer.concat([buf,d])
					})
					stdout.on('end',function(){
						console.log('enregistrement de l\'image de taille ' +buf.length) 
						equipe.image.push({data:buf,contentType:mimetype})
						equipe.save(function(err,equipe){
							if(err){
								return next(err)
							}
							console.log('image ajoutée')
						})
					})
				}

			})
			//enregistrement
			//				equipe.image.push({data:bufferFinal,contentType:mimetype})
		});
	});
	//règle de traitement des autres champs
	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
		equipe[fieldname]=val
		console.log('Field [' + fieldname + ']: value: ' + inspect(val));
	});
	//règle d'envoi à mangodb
	busboy.on('finish', function() {
		console.log('analyse terminée, envoi')
		equipe.save(function(err,equipe){
			if(err){
				return next(err)
			}
			console.log('enregistrement de base terminé ')
			res.status(201).json(equipe)
		})
	});
	//application du traitement par busboy
	req.pipe(busboy);
})

module.exports=router
