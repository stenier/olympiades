var express=require('express')
var router = express.Router()

//les options permettent de configurer les chemins relatifs de l'appli
var options = {
	root: __dirname + '/../layouts/',
	dotfiles: 'deny',
	headers: {
		'x-timestamp': Date.now(),
		'x-sent': true
	}
};

//fichiers statiques
router.use(express.static(__dirname + '/../assets'))
//templates angular
router.use(express.static(__dirname + '/../templates'))

router.get('/', function(req, res){
	res.sendFile('equipes.html',options)
})

router.get('/admin', function(req, res){
	res.sendFile('adminequipes.html',options)
})


module.exports=router
