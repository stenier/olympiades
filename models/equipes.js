var db = require('../db')

var Participant = new db.Schema({
	nom : {type : String, required:true}
	,nbEnfants : {type: Number, required:true}
	,nbAdultes : {type: Number, required:true}
        ,date:     { type: Date, required: true, default: Date.now }
})

var Image = new db.Schema({
	data : Buffer
	,contentType: String
})

var Equipe = new db.Schema({
	nom: {type : String, required:true}
	,completable: {type: Boolean, required: true, default: true}
	,image: [Image] //tableau pour pouvoir embbeder
	,participants: [Participant]
})

module.exports = db.model('Equipe',Equipe)
